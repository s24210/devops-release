## This script automates the deployment of a Go application using Kubernetes, Minikube, and Docker. Below are the steps it performs:

### IMPORTANT: 

Before executing this script, make sure you have the necessary dependencies installed 
(Minikube, Docker, kubectl, Go), and that the deployment.yaml and service.yaml files 
are correctly configured for your application.

### Downloading Go Modules
```shell
go get -t gitlab.com/enervalis-public/devops-release
```
Description: The following commands are used to download the required Go modules for the devops-release project hosted on GitLab:
### Testing the Application
```shell
go test -v ./...
```
Description: To ensure the functionality of the application, run the following command:
### Run Minikube:
```shell
minikube start
```
Description: Starts the Minikube virtual machine, which sets up a local Kubernetes cluster.
### Set up Docker for your local Minikube environment:
```shell
eval $(minikube docker-env)
```

Description: Configures Docker to use the Minikube environment, ensuring that images are built and available within the Minikube cluster.
### Build Docker Image:
```shell
docker build -t abb:1 .
```
Warning: This step requires changing the configuration in the application to make it open globally
Description: Builds a Docker image named abb with version 1 using the local files and configurations.
### Set up Kubernetes (Kube):
```shell
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```
Description: Applies the configurations defined in deployment.yaml and service.yaml to the Kubernetes cluster.
### Wait for Service to Start:
```shell
echo "Waiting for the service to up"
sleep 5
```
Description: Outputs a message indicating a wait for 5 seconds to allow the service to fully start.
### Check Deployment Status:
```shell
kubectl get deployments
```
Description: Retrieves and displays the status of the deployments to ensure they are running properly.
### Open the URL:
```shell
url=$(minikube service devops-release --url)
curl $url/vehicles
```
Description: Obtains the URL of the deployed service and uses curl to send an HTTP request to the /vehicles endpoint, retrieving the response.


