#!/bin/bash

# Download Go modules
go get -t gitlab.com/enervalis-public/devops-release

# Test application
go test -v ./...

# Run Minikube
minikube start

# Set up Docker for your local Minikube environment
eval $(minikube docker-env)

# Build Docker
docker build -t abb:1 .

# Set up kube
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

# Sleep for 5 s
echo "Waiting for the service to up"
sleep 15

# Check deployment status
kubectl get deployments

# Open the URL
url=$(minikube service devops-release --url)
curl $url/vehicles
